    </article>
<footer>
<?php if(!$printMode) {?>
    <a href="https://sourceforge.net/projects/mesonplayer/" class="sf-link">
        <img
            class="sf-logo"
            src="https://sflogo.sourceforge.net/sflogo.php?group_id=863771&type=16"
            width="120"
            height="30"
            alt="Get Meson Player at SourceForge.net. Fast, secure and Free Open Source software downloads"/>
    </a>
<?php } ?>
    <p class="alst-link">&copy; <a target="_blank" href="https://alkatrazstudio.net/">Alkatraz Studio</a>, 2014</p>
</footer>
</body>
</html>
